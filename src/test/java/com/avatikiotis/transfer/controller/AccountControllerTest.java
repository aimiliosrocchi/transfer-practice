package com.avatikiotis.transfer.controller;

import com.avatikiotis.transfer.exception.AccountExistingException;
import com.avatikiotis.transfer.exception.AccountNotFoundException;
import com.avatikiotis.transfer.exception.InsufficientBalanceException;
import com.avatikiotis.transfer.model.Account;
import com.avatikiotis.transfer.model.AccountUpdate;
import com.avatikiotis.transfer.model.ActionResult;
import com.avatikiotis.transfer.service.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

public class AccountControllerTest {

    @InjectMocks
    private AccountController accountController;

    @Mock
    private AccountService mockAccountService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_getAllAccounts() throws Exception {
        Date createdDate = new Date();
        Account acct1 = new Account(1L, 1000L, createdDate, "euro");
        Account acct2 = new Account(2L, 2000L, createdDate, "GBP");
        Account acct3 = new Account(3L, 3000L, createdDate, "euro");

        List<Account> allAccounts = Arrays.asList(acct1, acct2, acct3);

        ActionResult<List<Account>> actionResult = new ActionResult<List<Account>>(ActionResult.Action.RETRIEVE, allAccounts);
        ResponseEntity<ActionResult<List<Account>>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);
        CompletableFuture<ResponseEntity<ActionResult<List<Account>>>> result = CompletableFuture.completedFuture(response);

        when(mockAccountService.getAllAccounts()).thenReturn(allAccounts);

        CompletableFuture<ResponseEntity<ActionResult<List<Account>>>> expected = result;
        CompletableFuture<ResponseEntity<ActionResult<List<Account>>>> actual = accountController.getAllAccounts();

        verify(mockAccountService, times(1)).getAllAccounts();

        assertEquals(expected.get(), actual.get());
    }

    @Test
    public void test_getAccount() throws Exception {
        long id = 1L;
        Date craetedDate = new Date();
        Account account = new Account(id, 1000L, craetedDate, "euro");

        ActionResult<Account> actionResult = new ActionResult<Account>(ActionResult.Action.RETRIEVE, account);
        ResponseEntity<ActionResult<Account>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);
        CompletableFuture<ResponseEntity<ActionResult<Account>>> result = CompletableFuture.completedFuture(response);

        when(mockAccountService.getAccount(id)).thenReturn(account);

        CompletableFuture<ResponseEntity<ActionResult<Account>>> expected = result;
        CompletableFuture<ResponseEntity<ActionResult<Account>>> actual = accountController.getAccount(id);

        verify(mockAccountService, times(1)).getAccount(id);

        assertEquals(expected.get(), actual.get());
    }

    @Test(expected = AccountNotFoundException.class)
    public void test_getAccount_accountNotFound() throws Exception {
        long nonExistentId = 4L;

        AccountNotFoundException e = new AccountNotFoundException("Account with id " + nonExistentId + " not found");

        when(mockAccountService.getAccount(nonExistentId)).thenThrow(e);

        accountController.getAccount(nonExistentId);

        verify(mockAccountService, times(1)).getAccount(nonExistentId);
    }

    @Test
    public void test_createAccount() throws Exception {
        long id = 4L;
        float deposit = 4000L;
        String currency ="EURO";
        Date createdDate = new Date();
        Account account = new Account(id, deposit, createdDate, currency);

        ActionResult<Account> actionResult = new ActionResult<Account>(ActionResult.Action.CREATE, account);
        ResponseEntity<ActionResult<Account>> response = new ResponseEntity<>(actionResult, HttpStatus.CREATED);
        CompletableFuture<ResponseEntity<ActionResult<Account>>> result = CompletableFuture.completedFuture(response);

        when(mockAccountService.createAccount(id, deposit, createdDate, currency)).thenReturn(account);

        CompletableFuture<ResponseEntity<ActionResult<Account>>> expected = result;
        CompletableFuture<ResponseEntity<ActionResult<Account>>> actual = accountController.createAccount(id, deposit, createdDate, currency);

        verify(mockAccountService, times(1)).createAccount(id, deposit, createdDate, currency);

        assertEquals(expected.get(), actual.get());
    }

    @Test(expected = AccountExistingException.class)
    public void test_createAccount_accountAlreadyExists() throws Exception {
        long existingId = 2L;
        float deposit = 2000L;
        Date createdDate = new Date();

        AccountExistingException e = new AccountExistingException("Account with id " + existingId + " already exists");

        when(mockAccountService.createAccount(existingId, deposit, createdDate, "euro")).thenThrow(e);

        accountController.createAccount(existingId, deposit, createdDate, "euro");

        verify(mockAccountService, times(1)).createAccount(existingId, deposit, createdDate, "euro");
    }

    @Test
    public void test_deposit() throws Exception {
        long id = 1L;
        float balance = 1000L;
        float amount = 2000L;
        String currency = "TEST";
        AccountUpdate accountUpdate = new AccountUpdate(id, balance, balance + amount, currency);

        when(mockAccountService.deposit(id, amount)).thenReturn(accountUpdate);

        ActionResult<AccountUpdate> actionResult = new ActionResult<AccountUpdate>(ActionResult.Action.DEPOSIT, accountUpdate);
        ResponseEntity<ActionResult<AccountUpdate>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);
        CompletableFuture<ResponseEntity<ActionResult<AccountUpdate>>> result = CompletableFuture.completedFuture(response);

        CompletableFuture<ResponseEntity<ActionResult<AccountUpdate>>> expected = result;
        CompletableFuture<ResponseEntity<ActionResult<AccountUpdate>>> actual = accountController.deposit(id, amount);

        verify(mockAccountService, times(1)).deposit(id, amount);

        assertEquals(expected.get(), actual.get());
    }

    @Test(expected = AccountNotFoundException.class)
    public void test_deposit_accountNotFound() throws Exception {
        long nonExistentId = 4L;
        float amount = 4000L;

        AccountNotFoundException e = new AccountNotFoundException("Account with id " + nonExistentId + " not found");

        when(mockAccountService.deposit(nonExistentId, amount)).thenThrow(e);

        accountController.deposit(nonExistentId, amount);

        verify(mockAccountService, times(1)).deposit(nonExistentId, amount);
    }

    @Test
    public void test_withdraw() throws Exception {
        long id = 2L;
        float balance = 2000L;
        float amount = 1000L;
        String currency = "TEST";
        AccountUpdate accountUpdate = new AccountUpdate(id, balance, balance - amount, currency);

        when(mockAccountService.withdraw(id, amount)).thenReturn(accountUpdate);

        ActionResult<AccountUpdate> actionResult = new ActionResult<AccountUpdate>(ActionResult.Action.WITHDRAW, accountUpdate);
        ResponseEntity<ActionResult<AccountUpdate>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);
        CompletableFuture<ResponseEntity<ActionResult<AccountUpdate>>> result = CompletableFuture.completedFuture(response);

        CompletableFuture<ResponseEntity<ActionResult<AccountUpdate>>> expected = result;
        CompletableFuture<ResponseEntity<ActionResult<AccountUpdate>>> actual = accountController.withdraw(id, amount);

        verify(mockAccountService, times(1)).withdraw(id, amount);

        assertEquals(expected.get(), actual.get());
    }

    @Test(expected = AccountNotFoundException.class)
    public void test_withdraw_accountNotFound() throws Exception {
        long nonExistentId = 4L;
        float amount = 4000L;

        AccountNotFoundException e = new AccountNotFoundException("Account with id " + nonExistentId + " not found");

        when(mockAccountService.withdraw(nonExistentId, amount)).thenThrow(e);

        accountController.withdraw(nonExistentId, amount);

        verify(mockAccountService, times(1)).withdraw(nonExistentId, amount);
    }

    @Test(expected = InsufficientBalanceException.class)
    public void test_withdraw_insufficientBalance() throws Exception {
        long id = 2L;
        float balance = 2000L;
        float amount = 3000L;

        InsufficientBalanceException e = new InsufficientBalanceException("Insufficient balance: " + balance);

        when(mockAccountService.withdraw(id, amount)).thenThrow(e);

        accountController.withdraw(id, amount);

        verify(mockAccountService, times(1)).withdraw(id, amount);
    }

    @Test
    public void test_deleteAccount() throws Exception {
        long id = 3L;

        ActionResult<String> actionResult = new ActionResult<String>(ActionResult.Action.DELETE, "Account " + id + " Deleted");
        ResponseEntity<ActionResult<String>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);
        CompletableFuture<ResponseEntity<ActionResult<String>>> result = CompletableFuture.completedFuture(response);

        when(mockAccountService.deleteAccount(id)).thenReturn(true);

        CompletableFuture<ResponseEntity<ActionResult<String>>> expected = result;
        CompletableFuture<ResponseEntity<ActionResult<String>>> actual = accountController.deleteAccount(id);

        verify(mockAccountService, times(1)).deleteAccount(id);

        assertEquals(expected.get(), actual.get());
    }

    @Test(expected = AccountNotFoundException.class)
    public void test_deleteAccount_accountNotFound() throws Exception {
        long nonExistentId = 5L;

        AccountNotFoundException e = new AccountNotFoundException("Account with id " + nonExistentId + " not found");

        when(mockAccountService.deleteAccount(nonExistentId)).thenThrow(e);

        accountController.deleteAccount(nonExistentId);

        verify(mockAccountService, times(1)).deleteAccount(nonExistentId);
    }

}