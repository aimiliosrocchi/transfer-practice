package com.avatikiotis.transfer.controller;

import com.avatikiotis.transfer.exception.AccountNotFoundException;
import com.avatikiotis.transfer.exception.InsufficientBalanceException;
import com.avatikiotis.transfer.model.Account;
import com.avatikiotis.transfer.model.AccountUpdate;
import com.avatikiotis.transfer.model.ActionResult;
import com.avatikiotis.transfer.model.Transfer;
import com.avatikiotis.transfer.service.TransferService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.concurrent.CompletableFuture;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

public class TransferControllerTest {

    @InjectMocks
    private TransferController moneyTransferController;

    @Mock
    private TransferService mockMoneyTransferService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_transfer() throws Exception {
        long from = 1L;
        float fromBalance = 5000L;
        long to = 2L;
        long toBalance = 2000L;
        float amount = 3000L;
        String currency = "TEST";
        Date test = new Date();

        Account fromAcctInfo = new Account(from, fromBalance-amount, test, currency);
        Account toAcctInfo = new Account(to, toBalance+amount, test, currency);
        Transfer transfer = new Transfer(amount, Transfer.Status.SUCCESS, "Transfer Complete", fromAcctInfo, toAcctInfo, currency);
        ActionResult<Transfer> actionResult = new ActionResult<Transfer>(ActionResult.Action.TRANSFER, transfer);
        ResponseEntity<ActionResult<Transfer>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);
        CompletableFuture<ResponseEntity<ActionResult<Transfer>>> result = CompletableFuture.completedFuture(response);

        when(mockMoneyTransferService.transfer(from, to, amount, currency)).thenReturn(transfer);

        CompletableFuture<ResponseEntity<ActionResult<Transfer>>> expected = result;
        CompletableFuture<ResponseEntity<ActionResult<Transfer>>> actual = moneyTransferController.transfer(from, to, amount, currency);

        verify(mockMoneyTransferService, times(1)).transfer(from, to, amount, currency);

        assertEquals(expected.get(), actual.get());
    }

    @Test(expected = AccountNotFoundException.class)
    public void test_transfer_fromAccountNotFound() throws Exception {
        long nonExistentFrom = 4L;
        long to = 2L;
        float amount = 3000L;
        String currency = "euro";

        AccountNotFoundException e = new AccountNotFoundException("Account with id " + nonExistentFrom + " not found");

        when(mockMoneyTransferService.transfer(nonExistentFrom, to, amount, currency)).thenThrow(e);

        moneyTransferController.transfer(nonExistentFrom, to, amount, currency);

        verify(mockMoneyTransferService, times(1)).transfer(nonExistentFrom, to, amount, currency);
    }

    @Test(expected = AccountNotFoundException.class)
    public void test_transfer_toAccountNotFound() throws Exception {
        long from = 2L;
        long nonExistentTo = 5L;
        float amount = 1000L;
        String currency = "Euro";

        AccountNotFoundException e = new AccountNotFoundException("Account with id " + nonExistentTo + " not found");

        when(mockMoneyTransferService.transfer(from, nonExistentTo, amount, currency)).thenThrow(e);

        moneyTransferController.transfer(from, nonExistentTo, amount, currency);

        verify(mockMoneyTransferService, times(1)).transfer(from, nonExistentTo, amount, currency);
    }

    @Test(expected = InsufficientBalanceException.class)
    public void test_transfer_insufficientBalance() throws Exception {
        long from = 1L;
        float fromBalance = 1000L;
        long to = 3L;
        float amount = 5000L;
        String currency ="Euro";

        InsufficientBalanceException e = new InsufficientBalanceException("Insufficient balance: " + fromBalance);

        when(mockMoneyTransferService.transfer(from, to, amount, currency)).thenThrow(e);

        moneyTransferController.transfer(from, to, amount, currency);

        verify(mockMoneyTransferService, times(1)).transfer(from, to, amount, currency);
    }

}