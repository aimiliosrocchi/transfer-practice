package com.avatikiotis.transfer.service;

import com.avatikiotis.transfer.exception.AccountNotFoundException;
import com.avatikiotis.transfer.exception.SameAccountException;
import com.avatikiotis.transfer.model.Account;
import com.avatikiotis.transfer.model.AccountUpdate;
import com.avatikiotis.transfer.model.Transfer;
import com.avatikiotis.transfer.repository.AccountRepository;
import com.avatikiotis.transfer.service.impl.TransferServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

public class TransferServiceImplTest {

    @InjectMocks
    private TransferServiceImpl transferService;

    @Mock
    private AccountRepository mockAccountRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_transfer() {
        long fromId = 1L;
        float fromBalance = 5000L;
        long toId = 2L;
        float toBalance = 2000L;
        float amount = 3000L;
        Date createdDate = new Date();

        Account fromAccount = new Account(fromId, fromBalance-amount, createdDate, "test");
        Account toAccount = new Account(toId, toBalance+amount, createdDate, "test2");
        String currency = "TEST2";


        when(mockAccountRepository.findById(fromId)).thenReturn(Optional.of(fromAccount));
        when(mockAccountRepository.findById(toId)).thenReturn(Optional.of(toAccount));

        when(mockAccountRepository.save(fromAccount)).thenReturn(fromAccount);
        when(mockAccountRepository.save(toAccount)).thenReturn(toAccount);

        Transfer expected = new Transfer(
             amount, Transfer.Status.SUCCESS, "Transfer Complete", fromAccount, toAccount, currency);
        Transfer actual = transferService.transfer(fromId, toId, amount, currency);

        verify(mockAccountRepository, times(1)).findById(fromId);
        verify(mockAccountRepository, times(1)).findById(toId);
        verify(mockAccountRepository, times(1)).save(fromAccount);
        verify(mockAccountRepository, times(1)).save(toAccount);

        assertEquals(expected, actual);
    }

    @Test(expected = AccountNotFoundException.class)
    public void test_transfer_fromAccountNotFound() {
        long nonExistentFromId = 4L;
        long toId = 2L;
        float amount = 3000L;
        String currency = "test";
        when(mockAccountRepository.findById(nonExistentFromId)).thenReturn(Optional.empty());

        transferService.transfer(nonExistentFromId, toId, amount, currency);

        verify(mockAccountRepository, times(1)).findById(nonExistentFromId);
    }

    @Test(expected = AccountNotFoundException.class)
    public void test_transfer_toAccountNotFound() {
        long fromId = 1L;
        float fromBalance = 5000L;
        long nonExistentToId = 5L;
        float amount = 3000L;
        Date createdDate = new Date();

        Account fromAccount = new Account(fromId, fromBalance, createdDate, "test");

        when(mockAccountRepository.findById(fromId)).thenReturn(Optional.of(fromAccount));
        when(mockAccountRepository.findById(nonExistentToId)).thenReturn(Optional.empty());

        transferService.transfer(fromId, nonExistentToId, amount, "test");

        verify(mockAccountRepository, times(1)).findById(fromId);
        verify(mockAccountRepository, times(1)).findById(nonExistentToId);
    }

    @Test
    public void test_transfer_insufficientBalance() {
        long fromId = 1L;
        float fromBalance = 2000L;
        long toId = 2L;
        float toBalance = 5000L;
        float amount = 3000L;
        String currency = "TEST2";
        Date testDate = new Date();

        Account fromAccount = new Account(fromId, fromBalance, testDate, currency);
        Account toAccount = new Account(toId, toBalance,testDate, currency);



        when(mockAccountRepository.findById(fromId)).thenReturn(Optional.of(fromAccount));
        when(mockAccountRepository.findById(toId)).thenReturn(Optional.of(toAccount));

        Transfer expected = new Transfer(
              amount, Transfer.Status.FAILURE, "Insufficient Balance", fromAccount, toAccount, currency);
        Transfer actual = transferService.transfer(fromId, toId, amount, currency);

        verify(mockAccountRepository, times(1)).findById(fromId);
        verify(mockAccountRepository, times(1)).findById(toId);

        assertEquals(expected, actual);
    }

}