package com.avatikiotis.transfer.model;

import com.avatikiotis.transfer.exception.InsufficientBalanceException;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "ACCOUNT")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @JsonProperty
    private Long id;

    @JsonProperty
    @Column(name = "BALANCE")
    private float balance;

    @JsonProperty
    @Column(name = "CURRENCY")
    private String currency;

    @JsonProperty
    @Column(name = "CREATED_AT")
    private Date createdAt;


    public Account() {

    }

    public Account(long id, float deposit, Date createdAt, String currency) {
        this.id = id;
        this.balance = deposit;
        this.createdAt = createdAt;
        this.currency = currency;
    }

    public synchronized void addToBalance(float amount) {
        balance += amount;
    }

    public synchronized void subtractFromBalance(float amount) {
        if (balance < amount) {
            throw new InsufficientBalanceException("Insufficient balance: " + balance);
        }

        balance -= amount;
    }

    public synchronized boolean hasBalance() {
        return balance > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (o.getClass() != this.getClass()) return false;

        Account other = (Account) o;

        return this.id == other.id && this.balance == other.balance && this.currency == other.currency && this.createdAt == other.createdAt;
    }

    @Override
    public int hashCode() {
        int hash = 31;

        hash = hash * 17 + Long.hashCode(id);
        hash = hash * 17 + Float.hashCode(balance);

        return hash;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (Exception e) {
            return "Account[id=" + id + ", balance=" + balance + ", currency=" + currency + ", createdAt=" + createdAt + "]";
        }
    }

    public long getId() {
        return id;
    }

    public float getBalance() {
        return balance;
    }

    public Date getCreatedAt() {
        return createdAt;
    }


    public String getCurrency() {
        return currency;
    }


    public void setBalance(float balance) {
    }
}
