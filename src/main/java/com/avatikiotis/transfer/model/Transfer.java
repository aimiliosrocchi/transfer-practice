package com.avatikiotis.transfer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.*;

//transfer entity
@Entity
@Table(name = "TRANSFER")
public class Transfer {

    @JsonPropertyOrder({"status", "message", "from", "to", "amount", "from-account-info", "to-account-info"})

//using enums for status
    public enum Status {
        SUCCESS,
        FAILURE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @JsonProperty
    private Long id;

    @Column(name = "AMOUNT")
    @JsonProperty
    private float amount;
    @Column(name = "STATUS")
    @JsonProperty
    private Status status;

    @Column(name = "MESSAGE")
    @JsonProperty
    private String message;


    //one to one with account update table (extra)
    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty("from-account-info")
    private Account fromAccountInfo;

    //one to one with account update table (extra)
    @OneToOne(cascade = CascadeType.ALL)
    @JsonProperty("to-account-info")
    private Account toAccountInfo;

    @Column(name = "CURRENCY")
    @JsonProperty("currency")
    private String currency;

    public Transfer() {

    }

    public Transfer(
            float amount,
            Status status,
            String message,
            Account fromAccountInfo,
            Account toAccountInfo,
            String currency) {

        this.amount = amount;
        this.status = status;
        this.message = message;
        this.fromAccountInfo = fromAccountInfo;
        this.toAccountInfo = toAccountInfo;
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (o.getClass() != this.getClass()) return false;

        Transfer other = (Transfer) o;

        return this.amount == other.amount &&
                this.status == other.status &&
                this.message.equals(other.message) &&
                this.fromAccountInfo.equals(other.fromAccountInfo) &&
                this.toAccountInfo.equals(other.toAccountInfo) &&
                this.currency.equals(other.currency);

    }

    @Override
    public int hashCode() {
        int hash = 31;

        hash = hash * 17 + Float.hashCode(amount);
        hash = hash * 17 + status.hashCode();
        hash = hash * 17 + message.hashCode();
        hash = hash * 17 + fromAccountInfo.hashCode();
        hash = hash * 17 + toAccountInfo.hashCode();

        return hash;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (Exception e) {
            return "Transfer[amount=" + amount +
                    ", status=" + status + ", message=" + message +
                    ", fromAccountInfo=" + fromAccountInfo.toString() + ", toAccountInfo=" + toAccountInfo.toString() + ", currency=" + currency + "]";
        }
    }


    public float getAmount() {
        return amount;
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getCurrency() {
        return currency;
    }

    public Account getFromAccountInfo() {
        return fromAccountInfo;
    }

    public Account getToAccountInfo() {
        return toAccountInfo;
    }

}

