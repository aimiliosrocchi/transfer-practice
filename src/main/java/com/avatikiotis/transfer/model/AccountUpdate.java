package com.avatikiotis.transfer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.*;

import java.util.Objects;


public class AccountUpdate {


    @JsonProperty
    private Long id;
    @JsonProperty("account-id")
    private long accountId;

    @JsonProperty("balance-before")
    private float balanceBefore;

    @JsonProperty("balance-after")
    private float balanceAfter;

    @JsonProperty("CURRENCY")
    private String currency;

    public AccountUpdate() {

    }

    public AccountUpdate(long accountId, float currentBalance, String currency) {
        this(accountId, currentBalance, currentBalance, currency);
    }

    public AccountUpdate(long accountId, float balanceBefore, float balanceAfter, String currency) {
        this.accountId = accountId;
        this.balanceBefore = balanceBefore;
        this.balanceAfter = balanceAfter;
        this.currency = currency;

    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (o.getClass() != this.getClass()) return false;

        AccountUpdate other = (AccountUpdate) o;

        return this.accountId == other.accountId &&
                this.balanceBefore == other.balanceBefore &&
                this.balanceAfter == other.balanceAfter &&
                Objects.equals(this.currency, other.currency);
    }

    @Override
    public int hashCode() {
        int hash = 31;

        hash = hash*17 + Long.hashCode(accountId);
        hash = hash*17 + Float.hashCode(balanceBefore);
        hash = hash*17 + Float.hashCode(balanceAfter);

        return hash;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (Exception e) {
            return "AccountUpdate[id="+accountId+", balanceBefore="+balanceBefore+", balanceAfter="+balanceAfter+", +currency="+currency+ "]";
        }
    }

    public long getAccountId() {
        return accountId;
    }

    public float getBalanceBefore() {
        return balanceBefore;
    }

    public float getBalanceAfter() {
        return balanceAfter;
    }

    public String getCurrency() {
        return currency;
    }

    public void setBalanceAfter(float balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

}