package com.avatikiotis.transfer.controller;

import com.avatikiotis.transfer.model.Account;
import com.avatikiotis.transfer.model.AccountUpdate;
import com.avatikiotis.transfer.model.ActionResult;
import com.avatikiotis.transfer.service.AccountService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@Validated
@RequestMapping("/account")
@Async("threadPoolExecutor")
public class AccountController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    private AccountService accountService;

    //Method to get all accounts
    @RequestMapping("/all")
    public CompletableFuture<ResponseEntity<ActionResult<List<Account>>>> getAllAccounts() {
        LOGGER.info("getAllAccounts() handled by Thread-" + Thread.currentThread().getName());

        List<Account> allAccounts = accountService.getAllAccounts();
        ActionResult<List<Account>> actionResult = new ActionResult<List<Account>>(ActionResult.Action.RETRIEVE, allAccounts);
        ResponseEntity<ActionResult<List<Account>>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);

        return CompletableFuture.completedFuture(response);
    }

    //Get Account by id
    @RequestMapping("/id/{id}")
    public CompletableFuture<ResponseEntity<ActionResult<Account>>> getAccount(
            @PathVariable(value = "id", required = true) Long id) {

        LOGGER.info("getAccount() handled by Thread-" + Thread.currentThread().getName());

        Account account = accountService.getAccount(id);
        ActionResult<Account> actionResult = new ActionResult<Account>(ActionResult.Action.RETRIEVE, account);
        ResponseEntity<ActionResult<Account>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);

        return CompletableFuture.completedFuture(response);
    }


    //create account method
    @RequestMapping("/create/id/{id}/deposit/{deposit}/currency/{currency}")
    public CompletableFuture<ResponseEntity<ActionResult<Account>>> createAccount(
            @PathVariable(value = "id", required = true) Long id,
            @PathVariable(value = "deposit", required = true) Float deposit,
            Date createdDate, @PathVariable(value = "currency", required = false) String currency) {

        LOGGER.info("createAccount() handled by Thread-" + Thread.currentThread().getName());
        Date dateCreated = new Date();

        Account account = accountService.createAccount(id, deposit, dateCreated, currency);
        ActionResult<Account> actionResult = new ActionResult<Account>(ActionResult.Action.CREATE, account);
        ResponseEntity<ActionResult<Account>> response = new ResponseEntity<>(actionResult, HttpStatus.CREATED);

        return CompletableFuture.completedFuture(response);
    }

    //deposit to account method
    @RequestMapping("/deposit/id/{id}/amount/{amount}")
    public CompletableFuture<ResponseEntity<ActionResult<AccountUpdate>>> deposit(
            @PathVariable(value = "id", required = true) Long id,
            @PathVariable(value = "amount", required = true) Float amount) {

        LOGGER.info("deposit() handled by Thread-" + Thread.currentThread().getName());

        AccountUpdate accountUpdate = accountService.deposit(id, amount);
        ActionResult<AccountUpdate> actionResult = new ActionResult<AccountUpdate>(ActionResult.Action.DEPOSIT, accountUpdate);
        ResponseEntity<ActionResult<AccountUpdate>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);

        return CompletableFuture.completedFuture(response);
    }

    //withdraw from account method
    @RequestMapping("/withdraw/id/{id}/amount/{amount}")
    public CompletableFuture<ResponseEntity<ActionResult<AccountUpdate>>> withdraw(
            @PathVariable(value = "id", required = true) Long id,
            @PathVariable(value = "amount", required = true) Float amount) {

        LOGGER.info("withdraw() handled by Thread-" + Thread.currentThread().getName());

        AccountUpdate accountUpdate = accountService.withdraw(id, amount);
        ActionResult<AccountUpdate> actionResult = new ActionResult<AccountUpdate>(ActionResult.Action.WITHDRAW, accountUpdate);
        ResponseEntity<ActionResult<AccountUpdate>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);

        return CompletableFuture.completedFuture(response);
    }

    //delete from account method
    @RequestMapping("/delete/id/{id}")
    public CompletableFuture<ResponseEntity<ActionResult<String>>> deleteAccount(
            @PathVariable(value = "id", required = true) Long id) {

        LOGGER.info("deleteAccount() handled by Thread-" + Thread.currentThread().getName());

        accountService.deleteAccount(id);
        ActionResult<String> actionResult = new ActionResult<String>(ActionResult.Action.DELETE, "Account " + id + " Deleted");
        ResponseEntity<ActionResult<String>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);

        return CompletableFuture.completedFuture(response);
    }

}