package com.avatikiotis.transfer.controller;

import com.avatikiotis.transfer.model.ActionResult;
import com.avatikiotis.transfer.model.Transfer;
import com.avatikiotis.transfer.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
@RestController
@Validated
@RequestMapping("/transfer/v1")
@Async("threadPoolExecutor")
public class TransferController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    @Autowired
    private TransferService transferService;
    //Transfer money between two accounts
    @RequestMapping("/from/{from}/to/{to}/amount/{amount}/currency/{currency}")
    public CompletableFuture<ResponseEntity<ActionResult<Transfer>>> transfer(
            @PathVariable(value = "from", required = true) Long from,
            @PathVariable(value = "to", required = true) Long to,
            @PathVariable(value = "amount", required = true) Float amount,
            @PathVariable(value = "currency", required = true) String currency) {

        LOGGER.info("transfer() handled by Thread-" + Thread.currentThread().getName());
        Transfer transfer = transferService.transfer(from, to, amount, currency);
        ActionResult<Transfer> actionResult = new ActionResult<Transfer>(ActionResult.Action.TRANSFER, transfer);
        ResponseEntity<ActionResult<Transfer>> response = new ResponseEntity<>(actionResult, HttpStatus.OK);

        return CompletableFuture.completedFuture(response);
    }

}