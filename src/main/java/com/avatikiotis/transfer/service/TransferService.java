package com.avatikiotis.transfer.service;


import com.avatikiotis.transfer.model.Transfer;

public interface TransferService {

    Transfer transfer(long fromId, long toId, float amount, String currency);

}