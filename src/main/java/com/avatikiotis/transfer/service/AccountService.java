package com.avatikiotis.transfer.service;

import com.avatikiotis.transfer.model.Account;
import com.avatikiotis.transfer.model.AccountUpdate;

import java.util.Date;
import java.util.List;

public interface AccountService {

    List<Account> getAllAccounts();

    Account getAccount(long id);

    Account createAccount(long id, float deposit, Date dateCreated, String currency);

    AccountUpdate deposit(long id, float amount);

    AccountUpdate withdraw(long id, float amount);

    boolean deleteAccount(long id);

}