package com.avatikiotis.transfer.service.impl;

import com.avatikiotis.transfer.exception.AccountNotFoundException;
import com.avatikiotis.transfer.exception.InsufficientBalanceException;
import com.avatikiotis.transfer.exception.SameAccountException;
import com.avatikiotis.transfer.model.Account;
import com.avatikiotis.transfer.model.AccountUpdate;
import com.avatikiotis.transfer.model.Transfer;
import com.avatikiotis.transfer.repository.AccountRepository;
import com.avatikiotis.transfer.repository.TransferRepository;
import com.avatikiotis.transfer.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TransferServiceImpl implements TransferService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransferServiceImpl.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransferRepository transferRepository;

    @Override
    public Transfer transfer(long fromId, long toId, float amount, String currency) {
        Optional<Account> fromAccount = accountRepository.findById(fromId);

        //if account from doesn't exist return message to client
        if (!fromAccount.isPresent()) {
            return new Transfer(amount, Transfer.Status.FAILURE, "Transfer not completed - From Account not Found", null, null, currency);
        }

        Optional<Account> toAccount = accountRepository.findById(toId);
        //if account to doesn't exist return message to client
        if (!toAccount.isPresent()) {
            return new Transfer(amount, Transfer.Status.FAILURE, "Transfer not completed - TO Account not Found", null, null, currency);
        }

        //if same account return message to client
        if (fromId==toId) {
            return new Transfer(amount, Transfer.Status.FAILURE, "Transfer not completed - From Account is same with to Account", null, null, currency);
        }

        Account from = fromAccount.get();
        Account to = toAccount.get();



        try {
            from.subtractFromBalance(amount);
            to.addToBalance(amount);
            //save new balances to account table using crud repository
            accountRepository.save(from);
            accountRepository.save(to);

            from.setBalance(from.getBalance());
            to.setBalance(to.getBalance());
            Transfer transfer= new Transfer(amount, Transfer.Status.SUCCESS, "completed", from, to, currency);
           //save the transfer to transfer table using crud repository
            transferRepository.save(transfer);

            LOGGER.info(amount + " transferred from account " + fromId + " to account " + toId);

            //return success message to client
            return new Transfer(amount, Transfer.Status.SUCCESS, "Transfer Complete", from, to, currency);

        } catch (InsufficientBalanceException e) {
            //if balance is insufficient return message to client
            LOGGER.info("Insufficient balance in account " + fromId + ", aborting transfer ...");
            return new Transfer(amount, Transfer.Status.FAILURE, "Insufficient Balance", from, to, currency);
        }
    }

}