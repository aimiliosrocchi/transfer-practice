package com.avatikiotis.transfer.service.impl;

import com.avatikiotis.transfer.exception.AccountExistingException;
import com.avatikiotis.transfer.exception.AccountNotFoundException;
import com.avatikiotis.transfer.model.Account;
import com.avatikiotis.transfer.model.AccountUpdate;
import com.avatikiotis.transfer.repository.AccountRepository;
import com.avatikiotis.transfer.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Autowired
    private AccountRepository repository;


    //get all accounts
    @Override
    public List<Account> getAllAccounts() {
        List<Account> allAccounts = (List<Account>) repository.findAll();
        LOGGER.info("{} accounts found");
        return allAccounts;
    }

    //get account by id
    @Override
    public Account getAccount(long id) {
        Account account = findAccount(id);
        LOGGER.info("Account with id " + id + " found");
        return account;
    }

    //create account
    @Override
    public Account createAccount(long id, float amount, Date dateCrated, String currency) {
        Optional<Account> account = repository.findById(id);
        if (account.isPresent()) {
            throw new AccountExistingException("Account with id " + id + " already exists");
        }

        Account acct = new Account(id, amount, dateCrated, currency);
        repository.save(acct);

        LOGGER.info("Account created with id " + id + " with initial deposit " + amount);

        return acct;
    }

    //deposit to account given id and amount
    @Override
    public AccountUpdate deposit(long id, float amount) {
        Account account = findAccount(id);
        AccountUpdate accountUpdate = new AccountUpdate(id, account.getBalance(), account.getCurrency());

        account.addToBalance(amount);
        repository.save(account);

        LOGGER.info(amount + " deposited to account " + id);

        accountUpdate.setBalanceAfter(account.getBalance());

        return accountUpdate;
    }

    //withdraw from account given id and amount
    @Override
    public AccountUpdate withdraw(long id, float amount) {
        Account account = findAccount(id);
        AccountUpdate accountUpdate = new AccountUpdate(id, account.getBalance(), account.getCurrency());

        account.subtractFromBalance(amount);
        repository.save(account);

        LOGGER.info(amount + " withdrawn from account " + id);

        accountUpdate.setBalanceAfter(account.getBalance());

        return accountUpdate;
    }

    //delete account by id
    @Override
    public boolean deleteAccount(long id) {
        Account account = findAccount(id);

        if (account.hasBalance()) {
            LOGGER.info("Account " + id + " has remaining balance, first withdrawing remaining balance ...");
            withdraw(id, account.getBalance());
        }

        repository.delete(account);

        LOGGER.info("Account " + id + " deleted");

        return true;
    }

    //find account by id
    private Account findAccount(long id) {
        Optional<Account> account = repository.findById(id);
        if (!account.isPresent()) {
            throw new AccountNotFoundException("Account with id " + id + " not found");
        }
        return account.get();
    }

}