package com.avatikiotis.transfer.repository;

import com.avatikiotis.transfer.model.Account;
import com.avatikiotis.transfer.model.Transfer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransferRepository extends CrudRepository<Transfer, Long> {

}